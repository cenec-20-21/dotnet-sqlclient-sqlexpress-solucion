﻿using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Data.SqlClient;

namespace SqlServerConsole
{
    [SuppressMessage("ReSharper", "CommentTypo")]
    [SuppressMessage("ReSharper", "StringLiteralTypo")]
    [SuppressMessage("ReSharper", "IdentifierTypo")]
    class Program
    {
        static void Main(string[] args)
        {
            const string connectionString = "Data Source=localhost\\SQLEXPRESS;User Id=sa;Password=Pa$$w0rd;";
            const string databaseName = "EjemploSqlClient";
            const string tableOneName = "Alumno";
            const string tableTwoName = "Clase";
            const string tableJoinName = "Matricula";

            Console.WriteLine("Ejemplo de acceso a SQL Server con SqlClient provider");

            SqlConnection sqlConnection = null!;

            try
            {
                sqlConnection = new SqlConnection(connectionString);
                sqlConnection.Open();
                Console.WriteLine($"Conectado a SQL Server {sqlConnection.ServerVersion}");

                var sqlDropDatabase = $"DROP DATABASE IF EXISTS {databaseName};";
                var sqlDropDatabaseCommand = new SqlCommand(sqlDropDatabase, sqlConnection);
                sqlDropDatabaseCommand.ExecuteNonQuery();
                var sqlCreateDatabase = $"CREATE DATABASE {databaseName};";
                var sqlCreateDatabaseCommand = new SqlCommand(sqlCreateDatabase, sqlConnection);
                sqlCreateDatabaseCommand.ExecuteNonQuery();
                Console.WriteLine($"Base de datos {databaseName} re-creada");

                var sqlUseDatabase = $"USE {databaseName};";
                var sqlUseDatabaseCommand = new SqlCommand(sqlUseDatabase, sqlConnection);
                sqlUseDatabaseCommand.ExecuteNonQuery();
                Console.WriteLine($"Usando contexto de base de datos {databaseName}");

                var sqlCreateTableOne =
                    $"CREATE TABLE {tableOneName} ("
                    + "Id INT NOT NULL,"
                    + "Nombre VARCHAR(45) NOT NULL,"
                    + "Apellido VARCHAR(45) NOT NULL,"
                    + "PRIMARY KEY(Id))";
                var sqlCreateTableOneCommand = new SqlCommand(sqlCreateTableOne, sqlConnection);
                sqlCreateTableOneCommand.ExecuteNonQuery();
                Console.WriteLine($"Tabla {tableOneName} creada");

                var sqlCreateTableTwo =
                    $"CREATE TABLE {tableTwoName} ("
                    + "Id INT NOT NULL,"
                    + "Nombre VARCHAR(45) NOT NULL,"
                    + "Descripcion VARCHAR(MAX) NOT NULL,"
                    + "PRIMARY KEY(Id))";

                var sqlCreateTableTwoCommand = new SqlCommand(sqlCreateTableTwo, sqlConnection);
                sqlCreateTableTwoCommand.ExecuteNonQuery();
                Console.WriteLine($"Tabla {tableTwoName} creada");

                var sqlCreateTableJoin =
                    $"CREATE TABLE {tableJoinName} ("
                    + "Id INT NOT NULL,"
                    + "IdAlumno INT NOT NULL,"
                    + "IdClase INT NOT NULL,"
                    + "Fecha DATE NOT NULL,"
                    + "PRIMARY KEY(Id),"
                    + "FOREIGN KEY (IdAlumno) REFERENCES Alumno(Id),"
                    + "FOREIGN KEY (IdClase) REFERENCES Clase(Id))";
                var sqlCreateTableJoinCommand = new SqlCommand(sqlCreateTableJoin, sqlConnection);
                sqlCreateTableJoinCommand.ExecuteNonQuery();
                Console.WriteLine($"Tabla {tableJoinName} creada");

                // Ejercicio 1
                // Insertar alumnos Ted Codd, Martin Fowler, Greg Young y Udi Dahan
                var sqlInsertTableOne =
                    "INSERT INTO Alumno([Id],[Nombre],[Apellido]) VALUES"
                    + "(1, 'Ted', 'Codd'),"
                    + "(2, 'Martin', 'Fowler'),"
                    + "(3, 'Greg', 'Young'),"
                    + "(4, 'Udi', 'Dahan');";
                var sqlInsertTableOneCommand = new SqlCommand(sqlInsertTableOne, sqlConnection);
                sqlInsertTableOneCommand.ExecuteNonQuery();

                // Ejercicio 2
                // Insertar clases Sistemas Distribuidos, Bases de Datos, CQRS y Technical English
                var sqlInsertTableTwo =
                    "INSERT INTO Clase([Id],[Nombre],[Descripcion]) VALUES"
                    + "(1, 'Sistemas Distribuidos', 'Escalabilidad horizontal, mensajería y EDA'),"
                    + "(2, 'Bases de Datos', 'Cómo persistir y recuperar datos'),"
                    + "(3, 'CQRS', 'Conocimientos de Command Query Responsibility Segregation'),"
                    + "(4, 'Technical English', 'inglés técnico');";
                var sqlInsertTableTwoCommand = new SqlCommand(sqlInsertTableTwo, sqlConnection);
                sqlInsertTableTwoCommand.ExecuteNonQuery();

                // Ejercicio 3
                // Matricular a Ted Codd en Bases de datos en cualquier fecha de 1971
                // Matricular a Martin Fowler en Sistemas Distribuidos y Bases de datos en cualquier fecha de 2000
                // Matricular a Greg Young en Sistemas Distribuidos, Bases de datos y CQRS en cualquier fecha de 2003
                // Matricular a Udi Dahan en Sistemas Distribuidos, Bases de datos y CQRS en cualquier fecha de 2010
                var sqlInsertTableJoin =
                        "INSERT INTO Matricula([Id],[IdAlumno],[IdClase],[Fecha]) VALUES"
                        + "(1, 1, 2, '1971-1-1'),"
                        + "(2, 2, 1, '2000-1-1'),"
                        + "(3, 2, 2, '2000-1-1'),"
                        + "(4, 3, 1, '2003-1-1'),"
                        + "(5, 3, 2, '2003-1-1'),"
                        + "(6, 3, 3, '2003-1-1'),"
                        + "(7, 4, 1, '2010-1-1'),"
                        + "(8, 4, 2, '2010-1-1'),"
                        + "(9, 4, 3, '2010-1-1');";
                var sqlInsertTableJoinCommand = new SqlCommand(sqlInsertTableJoin, sqlConnection);
                sqlInsertTableJoinCommand.ExecuteNonQuery();

                // Ejercicio 4
                // Mostrar en consola todos los alumnos
                // Mostrar en consola todas las clases
                var sqlReadTableOne = "SELECT Nombre, Apellido FROM Alumno";
                Console.WriteLine("ALUMNOS:");
                var sqlReadTableOneQuery = new SqlCommand(sqlReadTableOne, sqlConnection);
                var alumnoSqlDataReader = sqlReadTableOneQuery.ExecuteReader();
                while (alumnoSqlDataReader.Read())
                {
                    var nombre = alumnoSqlDataReader.GetString(0);
                    var apellido = alumnoSqlDataReader.GetString(1);
                    Console.WriteLine($"{nombre} {apellido}");
                }
                alumnoSqlDataReader.Close();

                var sqlReadTableTwo = "SELECT Nombre FROM Clase";
                Console.WriteLine("CLASES:");
                var sqlReadTableTwoQuery = new SqlCommand(sqlReadTableTwo, sqlConnection);
                var claseSqlDataReader = sqlReadTableTwoQuery.ExecuteReader();
                while (claseSqlDataReader.Read())
                {
                    var nombre = claseSqlDataReader.GetString(0);
                    Console.WriteLine($"{nombre}");
                }
                claseSqlDataReader.Close();

                // Ejercicio 5
                // Mostrar en consola todas las clases y el número de alumnos que se han matriculado
                var sqlReadTableJoin =
                    "SELECT c.Nombre, COUNT(m.Id) as Total "
                    + "FROM Clase c LEFT JOIN Matricula m ON c.Id = m.IdClase "
                    + "GROUP BY c.Nombre;";
                Console.WriteLine("MATRICULACIONES:");
                var sqlReadTableJoinQuery = new SqlCommand(sqlReadTableJoin, sqlConnection);
                var matriculacionesSqlDataReader = sqlReadTableJoinQuery.ExecuteReader();
                while (matriculacionesSqlDataReader.Read())
                {
                    var nombre = matriculacionesSqlDataReader.GetString(0);
                    var total = matriculacionesSqlDataReader.GetInt32(1);
                    Console.WriteLine($"{nombre} {total}");
                }
                matriculacionesSqlDataReader.Close();

                // Ejercicio 6
                // Mostrar en consola todas las clases y el número de alumnos que se han matriculado antes de 2000
                var sqlReadTableJoinFiltered =
                        "SELECT c.Nombre, COUNT(m.Id) as Total "
                        + "FROM Clase c LEFT JOIN Matricula m ON c.Id = m.IdClase "
                        + "WHERE m.Fecha < '2000-1-1' "
                        + "GROUP BY c.Nombre;";
                Console.WriteLine("MATRICULACIONES ANTES DE 2000:");
                var sqlReadTableJoinFilteredQuery = new SqlCommand(sqlReadTableJoinFiltered, sqlConnection);
                var matriculacionesAntiguasSqlDataReader = sqlReadTableJoinFilteredQuery.ExecuteReader();
                while (matriculacionesAntiguasSqlDataReader.Read())
                {
                    var nombre = matriculacionesAntiguasSqlDataReader.GetString(0);
                    var total = matriculacionesAntiguasSqlDataReader.GetInt32(1);
                    Console.WriteLine($"{nombre} {total}");
                }
                matriculacionesAntiguasSqlDataReader.Close();

                // Ejercicio 7
                // Mostrar en consola todos los alumnos y el número de clases en el que se han matriculado
                var sqlReadTableJoinDetailed =
                        "SELECT a.Nombre, a.Apellido, COUNT(m.Id) as Total "
                        + "FROM Alumno a LEFT JOIN Matricula m ON a.Id = m.IdAlumno "
                        + "GROUP BY a.Nombre, a.Apellido;";
                Console.WriteLine("MATRICULACIONES DETALLADAS:");
                var sqlReadTableJoinDetailedQuery = new SqlCommand(sqlReadTableJoinDetailed, sqlConnection);
                var matriculacionesDetalladasSqlDataReader = sqlReadTableJoinDetailedQuery.ExecuteReader();
                while (matriculacionesDetalladasSqlDataReader.Read())
                {
                    var nombre = matriculacionesDetalladasSqlDataReader.GetString(0);
                    var apellido = matriculacionesDetalladasSqlDataReader.GetString(1);
                    var total = matriculacionesDetalladasSqlDataReader.GetInt32(2);
                    Console.WriteLine($"{nombre} {apellido} {total}");
                }
                matriculacionesDetalladasSqlDataReader.Close();

                Console.WriteLine("Presiona cualquier tecla para finalizar..");
                Console.ReadKey();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            finally
            {
                CloseConnection(sqlConnection);
            }
        }

        private static void CloseConnection(SqlConnection sqlConnection)
        {
            try
            {
                sqlConnection.Close();
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }
    }
}
